package com.login.task;

import com.login.task.model.*;
import com.login.task.repository.StudentRepository;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;

@SpringBootApplication
@RequiredArgsConstructor
@EnableConfigurationProperties
public class TaskApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(TaskApplication.class, args);
    }


    private final StudentRepository studentRepository;
    private final EntityManagerFactory entityManagerFactory;

    @Override
    public void run(String... args) throws Exception {


        Student student = new Student();
        student.setName("Cavansir");
        student.setSurname("Esedov");
        student.setAddress("Baki");
        studentRepository.save(student);

        Student student1 = new Student();
        student1.setName("Murad");
        student1.setSurname("Baqirov");
        student1.setAddress("Baki");
        studentRepository.save(student1);


        //JpaStreamer
        JPAStreamer jpaStreamer = JPAStreamer.of(entityManagerFactory);
        jpaStreamer.stream(Student.class)
                .filter(Student$.surname.endsWith("ov"))
                .filter(Student$.address.contains("Baki")).forEach(System.out::println);

//        JPAStreamer jpaStreamer = JPAStreamer.of(entityManagerFactory);
//
//        jpaStreamer.stream(Student.class)
//                .filter(Student$.address.equal("Baki"))
//                .filter(Student$.surname.contains("ov"))
//                .forEach(System.out::println);


//        studentRepository.test("Cavansir").stream().forEach(System.out::println);
//        studentRepository.getByEndTwoWordsAndCity("ov","Baki").stream().forEach(System.out::println);


//        Address address = new Address();
//        User user = new User();
//        UserRole userRole1 = new UserRole();
//        UserRole userRole2 = new UserRole();
//
//        user.setUsername("Cavansir");
//        user.setPassword("123457");
//
//        Contact contact1 = Contact.builder().contact_number("042565")
//                .build();
//        Contact contact2 = Contact.builder().contact_number("0425465165")
//                .build();
//        address.setName("Baku s Xetai r");
//
//        Set<Contact> contacts = new HashSet<>();
//        contacts.add(contact1);
//        contacts.add(contact2);
//        address.setContacts(contacts);
//
//        userRole1.setRole("Manager");
//        userRole2.setRole("Saler");
//        Set<UserRole> userRoleSet = new HashSet<>();
//        userRoleSet.add(userRole1);
//        userRoleSet.add(userRole2);
//        user.setUserRolesSet(userRoleSet);
//        user.setAddress(address);
//
//        userRepo.save(user);
//        addressRepo.save(address);


    }
}
