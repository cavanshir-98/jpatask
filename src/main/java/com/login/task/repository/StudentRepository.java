package com.login.task.repository;

import com.login.task.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long> {

////SQL
//    @Query(value = "select * from student where student.surname like %:name and student.address like %:city%",nativeQuery = true)
//    List<Student> getByEndTwoWordsAndCity(String name, String city);
//
//JPQL
//    @Query(value = "select s from Student s where s.surname like %:name and s.address=:city")
//     List<Student> getByEndTwoWordsAndCityJPQL(String name, String city);

    //JPAStreamer


//    @Query("select s from Student s where s.name like ?1%")
//    List<Student> test(String name);
}
