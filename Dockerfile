FROM alpine:3.11.2
RUN apk add --no-cache openjdk11
COPY build/libs/task-0.0.1-SNAPSHOT.jar /app/
CMD ["java","-jar", "/app/task-0.0.1-SNAPSHOT.jar"]